package sample;
import java.io.File;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

import javafx.stage.Stage;
import org.apache.commons.net.ftp.FTPFile;

import javax.swing.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Main extends Application {
    public Stage window;
    public BorderPane borderPane;
    public String buttenstyle;
    public Button Button1;
    public Button Button2;
    public Button Button3;
    public Button Friends;
    public Button Logout;
    public Boolean shows = false;
    public Button show;
    public VBox Left;
    public Label Subjekt;
    public TextField SubjektText;
    public Label Task;
    public  TextField TaskText;
    public  Label Date;
    public TextField DateText;
    public Button Subbmit;
    public ListView list;
    public Label Title;
    public Button DeleteHomeWorkbut;
    int SelectedLang;
    int intplus =0;
    public ComboBox ProdSelector;
    public ComboBox LangaugeSelector;
    public ComboBox InOrderSelector;
    public Button Start;
    public TextField FirstWord;
    public TextField SecundWord;
    public Button Correct;
    public  Label AmountOfWords;
    public BorderPane v;
    public DefaultListModel model = new DefaultListModel();
    public ListView FirstLangList;
    public ListView SecundLangList;
    public String path ="src/sample/Files/";
    public String user;
    public String userWithOut;
    public String pathtofiles;
    public String pathtohomework;
    public TextField usernam;
    public PasswordField passwd;
    public Label WrongMassage;
    public Scene scene;
    public ListView FriendsList;
    public String logoutString ="asdasdadaskjhdjasdbwauhgsjkdwakjhdbad";
    public Main() {
    }
    //Here does the user getting set
    public void setUser()throws Exception
    {
        Acount a = new Acount();
        BufferedReader br = new BufferedReader(new FileReader("src/sample/Files/UserInloged.txt"));
        a.setUsernam(br.readLine());
        user = a.getUsernam()+"/";
        userWithOut = a.getUsernam();
        pathtohomework= "src/sample/Files/"+user+"HomeWorks.txt";
        pathtofiles = "src/sample/Files/"+user;
        br.close();
    }
    private final double defaultFontSize = 32;
    //This function gets the files from the server
    public void GetFile(boolean first,boolean getUser,String User) throws Exception
    {
        FtpClient c = new FtpClient("ftp.stensatter.se","stensatter.se","J!imbo123");

        if(first)
        {
            try {
                byte[] buffer = c.getFile("alfred/Users.txt");
                FileOutputStream fos = new FileOutputStream(new File("src/sample/Files/Users.txt"));
                fos.write(buffer);
                fos.flush();
                System.out.println("Filen skrev till:" + "src/sample/Files/Users.txt");
            }
            catch (Exception e) {
                System.out.println("asd");
            }
        }
        if(getUser)
        {
            File theDir = new File(path+User+"/");
            if (!theDir.exists()) {
                System.out.println("creating directory: " + theDir.getName());
                boolean result = false;
                try {
                    theDir.mkdir();
                    result = true;
                } catch (SecurityException se) {
                    //handle it
                }
                if (result) {
                    System.out.println("DIR created");
                }
                FtpClient cc = new FtpClient("ftp.stensatter.se", "stensatter.se", "J!imbo123");

                List<FTPFile> dir = cc.list("alfred/" + User);
                for (FTPFile f : dir) {
                    cc = new FtpClient("ftp.stensatter.se", "stensatter.se", "J!imbo123");
                    //System.out.println("src/sample/Files/" + User + "/" + f.getName());
                    if (!f.getName().equals(".")) {
                        if (!f.getName().equals("..")) {
                            byte[] buffer = cc.getFile("alfred/" + User + "/" + f.getName());
                            System.out.println("src/sample/Files/" + User + "/" + f.getName());
                            System.out.println("alfred/" + User + "/" + f.getName());
                            FileOutputStream fos = new FileOutputStream(new File("src/sample/Files/" + User + "/" + f.getName()));
                            //System.out.println(buffer);
                            fos.write(buffer);
                            fos.flush();
                        }
                    }
                    cc.logout();
                }
            }
        }
    }

    public void UploadFile(boolean last,String path,String FromServerUser,String ToServerPath)throws Exception
    {
        setUser();
        FtpClient c = new FtpClient("ftp.stensatter.se","stensatter.se","J!imbo123");
        if(last)
        {
            String[] pathnames;
            File f = new File(path);
            pathnames = f.list();
            for(String pathname: pathnames)
            {
                if(!c.putFile("alfred/"+FromServerUser, new File(ToServerPath+pathname))){
                    System.out.println("Det gick fel!");
                }else{
                    System.out.println("Filen är uppladdad.");
                }
            }
            if(!c.putFile("alfred", new File(this.path+"Users.txt"))){
                System.out.println("Det gick fel!");
            }
            else{
                System.out.println("Filen är uppladdad.");
            }
        }
        c.logout();
    }
    //Login window
    @Override
    public void start(Stage primaryStage) throws Exception
    {
        window = primaryStage;
        window.setTitle("Log in to HomeWorker");
        borderPane = new BorderPane();
        scene = new Scene(borderPane,1920/2,1080/2);
        Label usernamlabel = new Label("Username:");
        Label passwdlabel = new Label("Password:");
        usernam = new TextField();
        passwd = new PasswordField();
        WrongMassage = new Label();

        CheckBox remeber = new CheckBox("Remember me");
        remeber.setOnMouseClicked(mouseEvent ->{
            try {
                FileWriter fw = new FileWriter(path+"rememberMe.txt");
                if(remeber.isSelected())
                {
                    fw.write(usernam.getText());
                }
                else
                {
                    fw.write(logoutString);

                }
                fw.close();

            } catch (IOException e) {
                e.printStackTrace();
            }

        });
        Button Login = new Button("Login");
        Login.setOnAction(e->{
            try {
                GetFile(false,true,usernam.getText());

                LogIn(usernam.getText(),passwd.getText() ,false);

            } catch (Exception ex) {
                ex.printStackTrace();

            }
        });
        Button ForgottenPas = new Button("Forgot your password or username?");
        Button SignUp = new Button("Sign Up");
        SignUp.setOnAction(e->{SignUp();});
        Login.setDefaultButton(true);

        HBox firstrow = new HBox();
        firstrow.setPadding(new Insets(10,10,10,10));
        firstrow.getChildren().addAll(usernamlabel,usernam);
        HBox secundrow = new HBox();
        secundrow.setPadding(new Insets(10,10,10,10));
        secundrow.getChildren().addAll(passwdlabel,passwd);
        HBox thirdrow = new HBox();
        thirdrow.setSpacing(10);
        thirdrow.setPadding(new Insets(10,10,10,10));
        thirdrow.getChildren().addAll(Login,ForgottenPas,SignUp,remeber);
        HBox fourthrow = new HBox();
        fourthrow.setPadding(new Insets(10,10,10,10));
        fourthrow.getChildren().addAll(WrongMassage);

        VBox vBox = new VBox();
        vBox.getChildren().addAll(firstrow,secundrow,thirdrow,fourthrow);
        vBox.setPadding(new Insets(150,150,300,300));
        borderPane.setCenter(vBox);

        window.setScene(scene);
        window.show();
        try {
            GetFile(true,false,"");
        } catch (Exception e) {
            System.out.println("Kan inte få filen");
            throw e;
        }
        BufferedReader br = new BufferedReader(new FileReader(path+"rememberMe.txt"));
        if(!br.readLine().equals(logoutString))
        {
            GetFile(false,false,"");
            setUser();
            MainWindow();
            CheckFriends(false,false,"");
            try {
                NotiFication();
            }
            catch (Exception e)
            {
                System.out.println("Inga re");
            }
        }
        br.close();
    }
    //This is the function that loges the user in
    public void LogIn(String username,String passwd,boolean rem) throws Exception
    {

        BufferedReader br = new BufferedReader(new FileReader(path+"Users.txt"));
        String currentline;

        while((currentline=br.readLine())!=null)
        {
            if(currentline.equals(username+","+passwd))
            {
                FileWriter fw = new FileWriter(path+"/UserInLoged.txt");
                fw.write(usernam.getText());
                fw.close();
                setUser();
                MainWindow();
                CheckFriends(false,false,"");
                try {
                    NotiFication();
                }
                catch (Exception e)
                {
                    System.out.println("Inga re");
                }
            }
            else {
                WrongMassage.setText("Wrong password");
            }
        }
    }

    public void CheckFriends(boolean agree,boolean dicline,String NewFriend) throws Exception
    {
        if(agree)
        {
            HBox hbox1 = new HBox();
            borderPane.setRight(hbox1);
            FileWriter fw = new FileWriter(pathtofiles+"FriendList.txt",true);
            fw.write(NewFriend+"\n");
            fw.close();
        }
        if(dicline)
        {
            HBox hbox1 = new HBox();
            borderPane.setRight(hbox1);
        }

    }

    public void NotiFication()throws Exception
    {
        int k = 0;

        String line = Files.readAllLines(Paths.get(pathtofiles+"FriendRequests.txt")).get(k);
        String Friend = line;
        HBox hbox1 = new HBox();
        VBox vBox = new VBox();

        Label l = new Label("Friendrequest");
        Label l1 = new Label(Friend+" has asked to be your friend");
        Button agree = new Button("Agree");
        agree.setOnAction(e-> {
            String stringToDelete = line;
            File file = new File(pathtofiles+"FriendRequests.txt");


            try {
                CheckFriends(true,false,Friend);
                BufferedReader br = new BufferedReader(new FileReader(file));
                String currentfriendRe;

                while ((currentfriendRe=br.readLine())!=null)
                {

                    if(!currentfriendRe.equals(stringToDelete))
                    {
                        System.out.println(currentfriendRe);
                        FileWriter fw = new FileWriter(pathtofiles+"FriendRequestsCopy.txt");
                        fw.write(currentfriendRe);
                        fw.close();
                    }
                }
                br.close();
                File filecopy = new File(pathtofiles+"FriendRequestsCopy.txt");

                if(file.delete())
                {

                }
                else
                {
                    System.out.println("dident");
                }

                if (filecopy.renameTo(new File("src/sample/Files/"+userWithOut+"/FriendRequests.txt"))) {
                }
                else
                {
                    FileWriter fw = new FileWriter("src/sample/Files/"+userWithOut+"/FriendRequests.txt");
                    fw.close();
                }
                try {
                    NotiFication();
                }
                catch (Exception h)
                {
                    // h.printStackTrace();
                }


            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        Button dicline = new Button("Dicline");
        dicline.setOnAction(e-> {
            try {
                CheckFriends(false,true,Friend);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        Button hide = new Button("Hide");
        l.setTextFill(Color.web("#0076a3"));
        //agree.setTextFill(Color.web("#4fff14"));
        agree.setStyle("-fx-background-color:#64EE64");
        dicline.setStyle("-fx-background-color:#ff2e17");

        hbox1.getChildren().addAll(l1,agree,dicline,hide);
        vBox.getChildren().addAll(l,hbox1);
        borderPane.setRight(vBox);
    }

    ListView FriendsHomeworkList;
    void GetFriendsHomeWorks()throws Exception
    {
        FriendsHomeworkList.getItems().clear();
        BufferedReader br = new BufferedReader(new FileReader(path+FriendsList.getSelectionModel().getSelectedItem()+"/HomeWorks.txt"));
        String currentline;
        while ((currentline=br.readLine())!=null)
        {
            FriendsHomeworkList.getItems().add(currentline);
        }
    }

    void AddFriend(String name) throws Exception
    {
     try {
         File theDir = new File(path+name+"/");
         if (!theDir.exists()) {
             System.out.println("creating directory: " + theDir.getName());
             boolean result = false;
             try {
                 theDir.mkdir();
                 result = true;
             } catch (SecurityException se) {
                 //handle it
             }
             if (result) {
                 System.out.println("DIR created");
             }
         }
             FtpClient cc = new FtpClient("ftp.stensatter.se", "stensatter.se", "J!imbo123");

             List<FTPFile> dir = cc.list("alfred/" + name);
             for (FTPFile f : dir) {
                 cc = new FtpClient("ftp.stensatter.se", "stensatter.se", "J!imbo123");

                 if (!f.getName().equals(".")) {
                     if (!f.getName().equals("..")) {
                         System.out.println(f.getName());
                         byte[] buffer = cc.getFile("alfred/" + name + "/" + f.getName());
                         try {
                             FileOutputStream fos = new FileOutputStream(new File("src/sample/Files/" + name + "/" + f.getName()));
                             fos.write(buffer);
                             fos.flush();
                         }
                         catch (Exception e)
                         {
                             //e.printStackTrace();
                            System.out.println("Skrev inte");
                         }

                     }
                 }
                 cc.logout();
                 FileWriter fw = new FileWriter(path+name+"/FriendRequests.txt");
                 fw.write(userWithOut);
                 fw.close();
                 FileWriter fw2 = new FileWriter(pathtofiles+"/FriendRequests.txt");
                 fw2.write(name);
                 fw2.close();
             }
         }
         catch (Exception e)
         {
             e.printStackTrace();
         }
     }

    public void FriendList() throws Exception
    {
        setButtonsDefultColor();
        buttenstyle = "-fx-background-color: rgba(22, 152, 199, 0.7);";
        Friends.setStyle(buttenstyle);

        VBox vbox = new VBox();
        VBox vbox1 = new VBox();
        VBox vbox2 = new VBox();

        HBox hbox1 = new HBox();
        HBox hbox2 = new HBox();
        HBox hbox3 = new HBox();
        HBox hbox4 = new HBox();

        Label label1 = new Label("Search for friends: ");
        TextField SearchField = new TextField();
        Button SearchButton = new Button("Search");
        SearchButton.setOnAction(e->{
            try {
                AddFriend(SearchField.getText());
            } catch (Exception ex) {
                ex.printStackTrace();
            }

        });


        Label label2 = new Label("Friends");
        Label label3 = new Label("Friends homeworks");

        FriendsList = new ListView();

        FriendsHomeworkList = new ListView();
        FriendsList.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                FriendsList.getSelectionModel().getSelectedItem();
                try {
                    GetFriendsHomeWorks();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });

        BufferedReader br = new BufferedReader(new FileReader(pathtofiles+"FriendList.txt"));
        String currentline;
        while ((currentline = br.readLine())!=null) {

            FriendsList.getItems().add(currentline);

        }
        br.close();

        Button Unfriend = new Button("Unfriend");
        Button RunTest = new Button("Run Vocabulary test");
        RunTest.setOnAction(e->{
            String[] name = FriendsHomeworkList.getSelectionModel().getSelectedItem().toString().split(",");
            Vocabularys(name[0]+" "+name[1]+" "+name[2]+".txt",FriendsList.getSelectionModel().getSelectedItem().toString()+"/");
        });

        hbox1.setSpacing(40);
        hbox1.setPadding(new Insets(50,0,0,100));
        hbox1.getChildren().addAll(label1,SearchField,SearchButton);
        hbox2.setSpacing(40);
        hbox2.setPadding(new Insets(20,20,20,20));
        hbox2.getChildren().addAll();
        hbox3.setPadding(new Insets(0,0,100,100));
        hbox3.setSpacing(40);
        hbox3.getChildren().addAll(vbox1,vbox2);
        vbox1.getChildren().addAll(label2,FriendsList,Unfriend);
        vbox2.getChildren().addAll(label3,FriendsHomeworkList,RunTest);


        vbox.getChildren().addAll(hbox1,hbox2,hbox3);
        borderPane.setCenter(vbox);

    }
    public void SignUp()
    {
        Label usernamlabel = new Label("Username:                  ");
        Label epostlabel = new Label("Epost:                         ");
        Label passwdlabel = new Label("Password:                   ");
        Label Secunfpasswdlabel = new Label("Write password again:");
        TextField usernamsign = new TextField();
        PasswordField passwdsign1 = new PasswordField();
        PasswordField passwdsign2 = new PasswordField();
        TextField epost = new TextField();
        WrongMassage = new Label();
        Button Subbmit = new Button("Subbmit");
        Button Back = new Button("Back");
        Back.setOnAction(e-> {
            try {
                start(window);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        Subbmit.setOnAction(e->{
            try
            {
                boolean is = true;
                if(usernamsign.getText().equals(""))
                {
                   WrongMassage.setText("Write username");
                }
                else if(epost.getText().equals(""))
                {
                    WrongMassage.setText("Write epost");
                }
                else if(passwdsign1.getText().equals(""))
                {
                    WrongMassage.setText("Write password");
                }
                else {
                    BufferedReader br = new BufferedReader(new FileReader(path+"Users.txt"));
                    String currentline;
                    while((currentline=br.readLine())!=null)
                    {
                        if(usernamsign.getText().equals(currentline.split(",")[0]))
                        {
                            WrongMassage.setText("This username is already in use");
                            is = false;
                            break;
                        }
                        else
                        {

                        }
                    }
                    if(is)
                    {
                        if(passwdsign1.getText().equals(passwdsign2.getText())){
                            SubbmitAcount(usernamsign.getText(),epost.getText(),passwdsign1.getText());
                            usernamsign.setText("");
                            epost.setText("");
                            passwdsign1.setText("");
                            passwdsign2.setText("");
                        }
                        else
                        {
                            WrongMassage.setText("Password doesnt match");
                        }
                    }

                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        Subbmit.setDefaultButton(true);

        HBox firstrow = new HBox();
        firstrow.setPadding(new Insets(10,10,10,10));
        firstrow.getChildren().addAll(usernamlabel,usernamsign);
        HBox secundrow = new HBox();
        secundrow.setPadding(new Insets(10,10,10,10));
        secundrow.getChildren().addAll(epostlabel,epost);
        HBox secundhalfrow = new HBox();
        secundhalfrow.setPadding(new Insets(10,10,10,10));
        secundhalfrow.getChildren().addAll(passwdlabel,passwdsign1);
        HBox secundunderrow = new HBox();
        secundunderrow.setPadding(new Insets(10,10,10,10));
        secundunderrow.getChildren().addAll(Secunfpasswdlabel,passwdsign2);

        HBox thirdrow = new HBox();
        thirdrow.setSpacing(10);
        thirdrow.setPadding(new Insets(10,100,100,100));
        thirdrow.getChildren().addAll(Back,Subbmit);
        HBox fourthrow = new HBox();
        fourthrow.setPadding(new Insets(10,10,10,10));
        fourthrow.getChildren().addAll(WrongMassage);

        VBox vBox = new VBox();
        vBox.getChildren().addAll(firstrow,secundrow,secundhalfrow,secundunderrow,thirdrow,fourthrow);
        vBox.setPadding(new Insets(150,150,300,300));
        borderPane.setCenter(vBox);



    }
    public void SubbmitAcount(String username,String Epost, String passwd) throws Exception
    {
        //This creates the folder
        File file = new File(path+username);
        boolean bool = file.mkdir();
        if(bool)
        {
            System.out.println("Maked");
        }
        //this creates the info file and the homeworks file
        FileWriter fw = new FileWriter(path+username+"/Info.txt");
        fw.write(passwd+"\n");
        fw.write(Epost);
        fw.close();
        FileWriter fw1 = new FileWriter(path+username+"/HomeWorks.txt");
        fw1.close();
        System.out.println(username+","+passwd);
        FileWriter fw2 = new FileWriter(path+"Users.txt",true);
        fw2.write(username+","+passwd+"\n");
        fw2.close();
        FileWriter fw3 = new FileWriter(path+username+"/FriendList.txt",true);
        fw3.close();
        FileWriter fw4 = new FileWriter(path+username+"/FriendRequests.txt",true);
        fw4.close();

        FtpClient c = new FtpClient("ftp.stensatter.se","stensatter.se","J!imbo123");
        boolean created = c.client.makeDirectory("alfred/"+username);

        if (created) {
            System.out.println("funka");
            UploadFile(true,path+username,username,path+username+"/");
            /*String[] pathnames;
            File f = new File(path+username);

            pathnames = f.list();
            for (String pathname : pathnames) {

                if(!c.putFile("alfred/"+username, new File(path+username+"/"+pathname+""))){
                    System.out.println("Det gick fel!"); }

                else{
                    System.out.println("Filen är uppladdad."); }

            }

        } else {
            System.out.println("funka inte");

            // something unexpected happened...*/
        }
        c.logout();
    }
    //This is the main window
    public void MainWindow()
    {

        window.setTitle("HomeWorker");
        String style = "-fx-background-color: rgba(44, 152, 199, 0.7);";
        buttenstyle = "-fx-background-color: rgba(180, 210, 222, 0.7);";
        String dotstyle ="-fx-text-fill: white; -fx-background-color: rgba(44, 152, 199, 0.7);";
        Left = new VBox(15);
        show = new Button(" . . . ");
        show.setStyle(dotstyle);
        show.setOnAction(e->GetSide());


        Left.setOnMouseEntered(e->GetSide());
        Left.setOnMouseExited(e->Removeside());
        Button1 = new Button("Home");

        Button1.setOnAction(e -> Home());
        Button1.setStyle(buttenstyle);
        Button2 = new Button("Vocabularys");

        Button2.setStyle(style);
        Button2.setOnAction(e -> Vocabularys("",user));

        Button3 = new Button("New Vocabularys");
        Button3.setStyle(style);
        Button3.setOnAction(e -> NewVocabularys("",false));

        Friends = new Button("Friends");
        Friends.setStyle(style);
        Friends.setOnAction(e -> {
            try {
                FriendList();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });

        Logout = new Button("Logout");
        Logout.setOnAction(e->{Logout();});


        Left.setStyle(style);
        Left.getChildren().addAll(show);


        borderPane.setLeft(Left);
        Home();

    }
    //This function logs user out
    public void Logout()
    {

        File f = new File(path+"rememberMe.txt");
        try {
            FileWriter fw = new FileWriter(f);
            fw.write(logoutString);
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            start(window);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //This function brings the menu
    private void GetSide()
    {
        if(!shows)
        {Left.getChildren().addAll(Button1,Button2,Button3,Friends,Logout);
            shows = true;
        }
        else{Removeside();}

    }
    //This removes the menu
    void Removeside()
    {
        Left.getChildren().removeAll(Button1,Button2,Button3,Friends,Logout);
        shows = false;
    }
    //This is the home screen
    public void Home()
    {
        setButtonsDefultColor();

        buttenstyle = "-fx-background-color: rgba(22, 152, 199, 0.7);";
        Button1.setStyle(buttenstyle);

        HBox hBox = new HBox();

        VBox vBox = new VBox();
        vBox.setPadding(new Insets(10,10,10,10));
        DeleteHomeWorkbut = new Button("Delete");
        DeleteHomeWorkbut.setOnAction(e-> {
            try {
                DeleteHomeWork();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });
        Subjekt = new Label("Subjekt");
        SubjektText = new TextField();
        Task = new Label("Task");
        TaskText = new TextField();
        TaskText.setPromptText("Vocabulary");
        Date = new Label("Date");
        DateText = new TextField();
        DateText.setPromptText("2020-01-01");
        Subbmit = new Button("Submit");
        Subbmit.setOnAction(e-> {
            try {
                SubmitHomeWork(SubjektText.getText(),TaskText.getText(),DateText.getText());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        Subbmit.setDefaultButton(true);
        list = new ListView();
        list.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent click) {
                if (click.getClickCount() == 2) {
                    String selected = list.getSelectionModel().getSelectedItem().toString();
                    File folder = new File(pathtofiles);
                    File[] listOfFiles = folder.listFiles();

                    String main ="src\\sample\\Files"+"\\"+userWithOut+"\\glos,"+selected.split(" ")[0]+","+selected.split(" ")[1]+","+selected.split(" ")[2]+".txt";
                    for (int i = 0; i < listOfFiles.length; i++) {
                        if (listOfFiles[i].isFile()&&listOfFiles[i].toString().equals(main)) {
                            Vocabularys(main.split(",")[1]+" "+main.split(",")[2]+" "+main.split(",")[3],user);

                        } else if (listOfFiles[i].isDirectory()) {
                            System.out.println("Directory " + listOfFiles[i].getName());
                        }
                    }
                }
            }
        });
        Title = new Label("        HomeWorker           ");
        DateText.setTextFormatter(new TextFormatter<>(change -> {
            if (change.getText().equals(" ")) {
                change.setText("-");
            }
            return change;
        }));
        try {
            ReadHomeWorks();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("ReadHomeWorks fel");

        }
        vBox.getChildren().addAll(list,DeleteHomeWorkbut,Subjekt,SubjektText,Task,TaskText,Date,DateText,Subbmit);
        hBox.setPadding(new Insets(10,10,10,10));
        hBox.getChildren().addAll(Title,vBox);
        /////SetFont();

        borderPane.setCenter(hBox);
    }

    public void DeleteVoc(String FileToRemove)
    {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Warning");
        alert.setHeaderText("Do you want to delete the Vocabulary test to?");
        ButtonType Yes = new ButtonType("Yes");
        ButtonType Cancel = new ButtonType("Cancel");
        alert.getButtonTypes().setAll(Yes, Cancel);
        Optional<ButtonType> result = alert.showAndWait();
        File file = new File(pathtofiles+"glos,"+FileToRemove+".txt");
        if (result.get() == Yes){

            if(file.delete())
            {
                System.out.println("Deleted");
            }
            else
            {
                System.out.println("Not Deleted");
            }
        }
        else {
            System.out.println("cancel");
        }
    }

    void DeleteHomeWork()throws IOException
    {

        String line = list.getSelectionModel().getSelectedItem().toString();
        String[] lineSplitted = line.split(" ");
        String lineToRemove = lineSplitted[0]+","+lineSplitted[1]+","+lineSplitted[2];
        File OriginalFile = new File(pathtohomework);
        File newFile = new File(pathtofiles+"HomeWorkscopy.txt");
        BufferedReader br = new BufferedReader(new FileReader(OriginalFile));
        String currentLine;
        System.out.println(lineToRemove);

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Warning");
        alert.setHeaderText("Do you want to delete the Homework?");
        ButtonType Yes = new ButtonType("Yes");
        ButtonType Cancel = new ButtonType("Cancel");
        alert.getButtonTypes().setAll(Yes, Cancel);

        Optional<ButtonType> result = alert.showAndWait();
        if (result.get() == Yes){
            list.getItems().remove(list.getSelectionModel().getSelectedIndex());
            while((currentLine=br.readLine())!=null) {

                if (!currentLine.equals(lineToRemove)) {
                    FileWriter fw = new FileWriter(newFile, true);
                    fw.write(currentLine+"\n");
                    fw.close();
                    System.out.println(currentLine);
                }
            }
            br.close();
            if(OriginalFile.delete())
            {
                System.out.println("Deleted");
            }
            else{
                System.out.println("not Deleted");
            }
            if(newFile.renameTo(new File(pathtofiles+"HomeWorks.txt")))
            {
                System.out.println("renamed");

            }
            else
            {
                System.out.println("not renamed");
            }
        }
        else {
            System.out.println("cancel");
        }
        if(line.contains("Vocabulary"))
        {
            DeleteVoc(lineToRemove);
        }
    }
    public void LoadVocProds() throws Exception
    {
        File folder = new File(pathtofiles);
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < listOfFiles.length; i++) {
            if (listOfFiles[i].isFile()&&listOfFiles[i].toString().contains("glos")) {
                String lastWord = listOfFiles[i].getName().split(",")[3];
                ProdSelector.getItems().add(listOfFiles[i].getName().split(",")[1]+" "+listOfFiles[i].getName().split(",")[2]+" "+listOfFiles[i].getName().split(",")[3]);

            } else if (listOfFiles[i].isDirectory()) {
                System.out.println("Directory " + listOfFiles[i].getName());
            }
        }

    }
    public void SubmitHomeWork(String firstname,String secundname,String thirdname)throws Exception
    {
        FileWriter fr = new FileWriter(pathtohomework,true);

        if(secundname.equals(""))
        {
            NewVocabularys(firstname+" "+"Vocabulary"+" "+thirdname,true);
            list.getItems().add(firstname+" "+"Vocabulary"+" "+thirdname);
            fr.write(firstname+","+"Vocabulary"+","+thirdname+"\n");
        }
        else
        {
            list.getItems().add(firstname+" "+secundname+" "+thirdname);
            fr.write(firstname+","+secundname+","+thirdname+"\n");

        }

        fr.close();
    }
    void ReadHomeWorks() throws Exception
    {
        BufferedReader br = new BufferedReader(new FileReader(pathtohomework));
        String currentLine;
        while((currentLine=br.readLine())!=null)
        {
            list.getItems().add(currentLine.split(",")[0]+" "+currentLine.split(",")[1]+" "+currentLine.split(",")[2]);

        }
        br.close();
    }

    ArrayList<String> WrongAnswerWrote = new ArrayList<String>();
    ArrayList<Integer> WrongAnswerLine = new ArrayList<Integer>();
    int trys = 3;

    void WrongAnswer()
    {
        WrongMassage.setText("wrong");
        WrongMassage.setStyle("-fx-font-size: 30;");
        WrongMassage.setTextFill(Color.web("#ff1100"));

        new java.util.Timer().schedule(
                new java.util.TimerTask() {
                    @Override
                    public void run() {
                        try {
                            WrongMassage.setStyle("-fx-font-size: 12;");
                            WrongMassage.setTextFill(Color.web("#ff1100"));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                150
        );

    }

    void FinishTest()
    {
        System.out.println("finish");
    }

    void CorrectWord(String path,Boolean skip)throws Exception
    {

        WrongMassage.setTextFill(Color.web("#29ff03"));
        System.out.println(path);
        String line = Files.readAllLines(Paths.get(path)).get(intplus);
        String Secundline = Files.readAllLines(Paths.get(path)).get(intplus+1);
        String[] linetoprint = line.split(",");
        String[] Secundlinetoprint = Secundline.split(",");
        if(SelectedLang == 0)
        {
            if(skip)
            {
                FirstWord.setText(Secundlinetoprint[SelectedLang]);
                intplus++;
                AmountOfWords.setText(toString().valueOf(intplus));
                SecundWord.setText("");
            }
            else {
                if (SecundWord.getText().equals(linetoprint[SelectedLang + 1])) {
                    FirstWord.setText(Secundlinetoprint[SelectedLang]);
                    intplus++;
                    AmountOfWords.setText(toString().valueOf(intplus));
                    SecundWord.setText("");
                    trys=3;
                    WrongMassage.setText("Right");

                } else {
                    if(trys ==0)
                    {
                        WrongAnswerWrote.add(SecundWord.getText());
                        FirstWord.setText(Secundlinetoprint[SelectedLang]);
                        intplus++;
                        AmountOfWords.setText(toString().valueOf(intplus));
                        SecundWord.setText("");
                        trys=3;
                        WrongMassage.setText("");

                    }
                    WrongAnswer();
                    System.out.println("Wrong");
                    trys--;
                }
            }
        }
        else
        {
            if(skip)
            {
                FirstWord.setText(Secundlinetoprint[SelectedLang]);
                intplus++;
                AmountOfWords.setText(toString().valueOf(intplus));
                SecundWord.setText("");
            }
            else {
                if (SecundWord.getText().equals(linetoprint[SelectedLang - 1])) {
                    FirstWord.setText(Secundlinetoprint[SelectedLang]);
                    intplus++;
                    AmountOfWords.setText(toString().valueOf(intplus));
                    SecundWord.setText("");
                    trys=3;
                    WrongMassage.setText("Right");

                } else {
                    if(trys ==0)
                    {
                        WrongAnswerWrote.add(SecundWord.getText());
                        FirstWord.setText(Secundlinetoprint[SelectedLang]);
                        intplus++;
                        AmountOfWords.setText(toString().valueOf(intplus));
                        SecundWord.setText("");
                        trys=3;
                        WrongMassage.setText("");

                    }
                    WrongAnswer();
                    System.out.println("Wrong");
                    trys--;                }
            }
        }
    }
    void StartTest(String path)throws Exception
    {
        intplus =0;
        System.out.println(path);
        File file = new File(path);
        String line = Files.readAllLines(Paths.get(path)).get(intplus);
        if(SelectedLang ==0)
        {
            FirstWord.setText(line.split(",")[SelectedLang]);
        }
        else
        {
            FirstWord.setText(line.split(",")[SelectedLang]);
        }

    }
    void Vocabularys(String name,String userr)
    {
        setButtonsDefultColor();
        buttenstyle = "-fx-background-color: rgba(22, 152, 199, 0.7);";
        Button2.setStyle(buttenstyle);
        VBox vbox = new VBox();

        HBox hBox1 = new HBox();
        ProdSelector = new ComboBox();
        ProdSelector.setValue(name);
        LangaugeSelector = new ComboBox();
        LangaugeSelector.getItems().addAll("First language","Secund language");
        LangaugeSelector.setValue("First language");
        LangaugeSelector.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                SelectedLang =LangaugeSelector.getSelectionModel().getSelectedIndex();
            }
        });

        InOrderSelector = new ComboBox();
        Start = new Button("Start");
        Start.setOnAction(e-> {
            try {
                StartTest(path+userr+"glos,"+ProdSelector.getSelectionModel().getSelectedItem().toString().split(" ")[0]+","+ProdSelector.getSelectionModel().getSelectedItem().toString().split(" ")[1]+","+ProdSelector.getSelectionModel().getSelectedItem().toString().split(" ")[2]);
                intplus = 0;
                AmountOfWords.setText(toString().valueOf(intplus));

            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        hBox1.getChildren().addAll(ProdSelector,LangaugeSelector,InOrderSelector,Start);
        hBox1.setSpacing(30);


        HBox hBox2 = new HBox(22);
        FirstWord = new TextField();
        SecundWord = new TextField();
        Correct = new Button("Correct");
        Correct.setDefaultButton(true);
        Correct.setOnAction(e-> {
            try {
                CorrectWord(path+userr+"glos,"+ProdSelector.getSelectionModel().getSelectedItem().toString().split(" ")[0]+","+ProdSelector.getSelectionModel().getSelectedItem().toString().split(" ")[1]+","+ProdSelector.getSelectionModel().getSelectedItem().toString().split(" ")[2],false);
            } catch (Exception ex) {
                AmountOfWords.setText(toString().valueOf(intplus+1));
                FirstWord.setText("");
                SecundWord.setText("");
            }
        });
        Button skipWord = new Button("Skip");
        skipWord.setOnAction(e-> {
            try {
                CorrectWord(path+userr+"glos,"+ProdSelector.getSelectionModel().getSelectedItem().toString().split(" ")[0]+","+ProdSelector.getSelectionModel().getSelectedItem().toString().split(" ")[1]+","+ProdSelector.getSelectionModel().getSelectedItem().toString().split(" ")[2],true);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        AmountOfWords = new Label("0");
        WrongMassage = new Label("");
        hBox2.getChildren().addAll(FirstWord,SecundWord,Correct,skipWord,AmountOfWords,WrongMassage);
        //hBox2.setSpacing(30);

        vbox.setPadding(new Insets(100,100,100,10));
        vbox.setSpacing(100);
        vbox.getChildren().addAll(hBox1,hBox2);
        try {
            LoadVocProds();
        } catch (Exception e) {
            e.printStackTrace();
        }

        borderPane.setCenter(vbox);

    }
    void MakeNewVocProd(String FirstWordd,String SecundWordd,String path)throws Exception
    {

        FileWriter fw = new FileWriter(path,true);
        fw.write(FirstWordd+","+SecundWordd+"\n");
        fw.close();
        FirstLangList.getItems().add(FirstWordd);
        SecundLangList.getItems().add(SecundWordd);


    }

    public int once;

    public void NewVocabularys(String SelectedName,boolean fromHome)
    {
        setButtonsDefultColor();
        buttenstyle = "-fx-background-color: rgba(22, 152, 199, 0.7);";
        Button3.setStyle(buttenstyle);
        Subbmit.setDefaultButton(false);
        once = 0;
        VBox vboxNV = new VBox();


        HBox hBox1NV = new HBox();
        Label NameLabel = new Label("Name");
        Label TypeLabel = new Label("Type");
        Label DateLabel = new Label("Date");
        TextField FirstNameTextfield= new TextField();
        TextField SecundNameTextfield= new TextField();
        TextField ThirdNameTextfield= new TextField();
        try{
            FirstNameTextfield.setText(SelectedName.split(" ")[0]);
            SecundNameTextfield.setText(SelectedName.split(" ")[1]);
            ThirdNameTextfield.setText(SelectedName.split(" ")[2]);
        }
        catch (Exception h)
        {
        }

        VBox vBox1 = new VBox();
        vBox1.getChildren().addAll(NameLabel,FirstNameTextfield);
        VBox vBox2 = new VBox();
        vBox2.getChildren().addAll(TypeLabel,SecundNameTextfield);
        VBox vBox3 = new VBox();
        vBox3.getChildren().addAll(DateLabel,ThirdNameTextfield);

        hBox1NV.getChildren().addAll(vBox1,vBox2,vBox3);
        hBox1NV.setSpacing(30);

        HBox hBox2NV = new HBox(22);
        Label Words = new Label("Words");
        TextField FirstWordTextfield= new TextField();
        TextField SecundWordTextfield= new TextField();
        Button AddWordsButton = new Button("Add");
        AddWordsButton.setDefaultButton(true);
        AddWordsButton.setOnAction(e->{
            try {
                if(FirstNameTextfield.getText().equals("")||ThirdNameTextfield.getText().equals(""))
                {
                    Alert WrongName = new Alert(Alert.AlertType.ERROR);
                    WrongName.setTitle("Error Dialog");
                    WrongName.setHeaderText("You need to fill in name and date");
                    WrongName.showAndWait();
                    once=1;
                }

                else if(!FirstNameTextfield.getText().equals("")||!ThirdNameTextfield.getText().equals(""))
                {
                    SecundNameTextfield.setText("Vocabulary");

                    MakeNewVocProd(FirstWordTextfield.getText(),SecundWordTextfield.getText(),pathtofiles+"glos,"+FirstNameTextfield.getText()+","+SecundNameTextfield.getText()+","+ThirdNameTextfield.getText()+".txt");
                    FirstWordTextfield.setText("");
                    SecundWordTextfield.setText("");
                    once=0;
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
            if(once==0)
            {
                if(!fromHome)
                {
                    Alert alert = new Alert(Alert.AlertType.INFORMATION);
                    alert.setTitle("Warning");
                    alert.setHeaderText("Do you want to add this as a homework in the homework list?");
                    ButtonType Yes = new ButtonType("Yes");
                    ButtonType Cancel = new ButtonType("Cancel");
                    alert.getButtonTypes().setAll(Yes, Cancel);
                    Optional<ButtonType> result = alert.showAndWait();
                    if (result.get() == Yes){

                        try {
                            if(FirstNameTextfield.getText().equals(""))
                            {
                                Alert WrongName = new Alert(Alert.AlertType.ERROR);
                                WrongName.setTitle("Error Dialog");
                                WrongName.setHeaderText("You need to fill in a name");
                                WrongName.showAndWait();
                            }
                            else if(SecundNameTextfield.getText().equals(""))
                            {
                                SecundNameTextfield.setText("Vocabulary");
                            }
                            else if(ThirdNameTextfield.getText().equals(""))
                            {
                                Alert WrongName = new Alert(Alert.AlertType.ERROR);
                                WrongName.setTitle("Error Dialog");
                                WrongName.setHeaderText("You need to fill in a date");
                                WrongName.showAndWait();
                            }
                            else
                            {
                                SubmitHomeWork(FirstNameTextfield.getText(),SecundNameTextfield.getText(),ThirdNameTextfield.getText());
                            }
                        } catch (Exception ex) {

                            ex.printStackTrace();
                        }

                    }
                    else {
                        System.out.println("cancel");
                    }}
                once=1;
            }


        });
        hBox2NV.getChildren().addAll(Words,FirstWordTextfield,SecundWordTextfield,AddWordsButton);
        HBox hBox3NV = new HBox(2);
        FirstLangList = new ListView();
        SecundLangList = new ListView();
        hBox3NV.setSpacing(1);
        hBox3NV.getChildren().addAll(FirstLangList,SecundLangList);
        vboxNV.setPadding(new Insets(100,100,100,10));
        vboxNV.setSpacing(100);
        vboxNV.getChildren().addAll(hBox1NV,hBox2NV,hBox3NV);

        borderPane.setCenter(vboxNV);
    }
    public void setButtonsDefultColor()
    {
        buttenstyle = "-fx-background-color: rgba(182, 210, 222, 0.7);";
        Button1.setStyle(buttenstyle);
        Button2.setStyle(buttenstyle);
        Button3.setStyle(buttenstyle);
        Friends.setStyle(buttenstyle);
        Logout.setStyle(buttenstyle);

    }

    public static void main(String[]args)
    {
        launch(args);
        Main a = new Main();

        try {
            a.setUser();
            a.UploadFile(true,a.pathtofiles,a.userWithOut,"src/sample/Files/"+a.user);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
