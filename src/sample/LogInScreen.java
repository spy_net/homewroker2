package sample;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.Window;

import java.io.BufferedReader;
import java.io.FileReader;

public class LogInScreen extends Application {

    public String path ="src/sample/Files/";
    public TextField usernam;
    public PasswordField passwd;
    public Label WrongMassage;
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        Stage window = primaryStage;
        window.setTitle("Log in to HomeWorker");
        BorderPane borderPane = new BorderPane();
        Scene scene = new Scene(borderPane,566,344);
        Label usernamlabel = new Label("Username:");
        Label passwdlabel = new Label("Password:");
        usernam = new TextField();
        passwd = new PasswordField();
        WrongMassage = new Label();
        Button Login = new Button("Login");
        Login.setOnAction(e->{
            try {
                LogIn();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        Button ForgottenPas = new Button("Forgot Info.txt?");
        Button SignUp = new Button("Sign Up");
        Login.setDefaultButton(true);

        HBox firstrow = new HBox();
        firstrow.setPadding(new Insets(10,10,10,10));
        firstrow.getChildren().addAll(usernamlabel,usernam);
        HBox secundrow = new HBox();
        secundrow.setPadding(new Insets(10,10,10,10));
        secundrow.getChildren().addAll(passwdlabel,passwd);
        HBox thirdrow = new HBox();
        thirdrow.setPadding(new Insets(10,10,10,10));
        thirdrow.getChildren().addAll(Login,ForgottenPas,SignUp);
        HBox fourthrow = new HBox();
        fourthrow.setPadding(new Insets(10,10,10,10));
        fourthrow.getChildren().addAll(WrongMassage);

        VBox vBox = new VBox();
        vBox.getChildren().addAll(firstrow,secundrow,thirdrow,fourthrow);
        vBox.setPadding(new Insets(100,100,100,100));
        borderPane.setCenter(vBox);

        window.setScene(scene);
        window.show();
    }

    public void LogIn() throws Exception
    {   try{        BufferedReader br = new BufferedReader(new FileReader(path+usernam.getText()+"/Info.txt"));
            if(br.readLine().equals(passwd.getText()))
            {
                System.out.println("Worked");
                Main m = new Main();

            }
            else{
                WrongMassage.setText("Wrong password");
                System.out.println("Didnt work "+br.readLine()+" "+path+usernam.getText()+"\\Info.txt");
            }
        }
        catch (Exception e)
        {
            WrongMassage.setText("Wrong Username");
            System.out.println("Wrong username");
        }


    }
}
